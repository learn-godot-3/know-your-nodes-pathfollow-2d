extends Control


export (NodePath) var dropdown_path
onready var dropdown_transitionTypes = get_node("DropDownTransitionTypes")
onready var dropdown_easingTypes = get_node("DropDownEasingTypes")


signal changeTransitionType
signal changeEasingType


# Keys and values for the dropdown menu.
var transitionTypeOptions =  {
	"TRANS_LINEAR": Tween.TRANS_LINEAR, 
	"TRANS_SINE":  Tween.TRANS_SINE,
	"TRANS_QUINT": Tween.TRANS_QUINT, 
	"TRANS_QUART": Tween.TRANS_QUART,
	"TRANS_QUAD": Tween.TRANS_QUAD,
	"TRANS_EXPO": Tween.TRANS_EXPO,
	"TRANS_ELASTIC": Tween.TRANS_ELASTIC,
	"TRANS_CUBIC": Tween.TRANS_CUBIC,
	"TRANS_CIRC": Tween.TRANS_CIRC,
	"TRANS_BOUNCE": Tween.TRANS_BOUNCE,
	"TRANS_BACK": Tween.TRANS_BACK
}


var easingTypeOptions = {
	"EASE_IN": Tween.EASE_IN,
	"EASE_IN_OUT": Tween.EASE_IN_OUT,
	"EASE_OUT": Tween.EASE_OUT,
	"EASE_OUT_IN": Tween.EASE_OUT_IN
}


func _ready():
	add_items_transition_types()
	add_items_easing_types()
	dropdown_transitionTypes.connect("item_selected", self, "_on_DropDownTransitionTypes_item_selected")
	dropdown_easingTypes.connect("item_selected", self, "_on_DropDownEasingTypes_item_selected")	
	

# Adding items to transitionType dropdown.
func add_items_transition_types():
	var keys = transitionTypeOptions.keys()
	var num_options = keys.size()
	
	for id in range(num_options):
		var key = keys[id]
		dropdown_transitionTypes.add_item(key, id)
		dropdown_transitionTypes.set_item_metadata(id, transitionTypeOptions[key])
		
		
# Add items to easingType dropdown.
func add_items_easing_types():
	var keys = easingTypeOptions.keys()
	var num_options = keys.size()
	
	for id in range(num_options):
		var key = keys[id]
		dropdown_easingTypes.add_item(key, id)
		dropdown_easingTypes.set_item_metadata(id, easingTypeOptions[key])


# Get the value of the selected drop down item and sends it with the
# custom signal "changeTransitionType".
func _on_DropDownTransitionTypes_item_selected(_index):
	var transitionType = dropdown_transitionTypes.get_selected_metadata()
	emit_signal("changeTransitionType", transitionType)
	

func _on_DropDownEasingTypes_item_selected(_index):
	var easingType = dropdown_easingTypes.get_selected_metadata()
	emit_signal("changeEasingType", easingType)
