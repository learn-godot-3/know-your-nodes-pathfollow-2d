# PathFollow 2d
My take on the video [Godot Engine - Know Your Nodes: PathFollow2D](https://www.youtube.com/watch?v=_lJ0jbahbjw&list=PLsk-HSGFjnaE4k-X3l6dZAPIoB2QB4eYg&index=4&t=314s) by the Channel [KidsCanCode](https://www.youtube.com/channel/UCNaPQ5uLX5iIEHUCLmfAgKg)

It implements a little spaceship that follows a circular path animated in a tween.
You can select the tween's transition and easing.

![Example](images/path-follow-2d-example.png)
