extends Path2D


onready var follow = $follow
var tween

var transitionType = Tween.TRANS_LINEAR
var easingType = Tween.EASE_IN

# Called when the node enters the scene tree for the first time.
func _ready():
	tween = Tween.new()
	add_child(tween)
	# tween.interpolate_property(follow, "unit_offset", 0, 1, 6, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	interpolate_property()
	tween.set_repeat(true)
	tween.start()

# Re-start the tween with new transition and easing types.
func interpolate_property():
	tween.interpolate_property(follow, "unit_offset", 0, 1, 6, transitionType, easingType)

# Set the transition type
func set_transition_type(value):
	transitionType = value
	interpolate_property()

# Set the easing type
func set_easing_type(value):
	easingType = value
	interpolate_property()
