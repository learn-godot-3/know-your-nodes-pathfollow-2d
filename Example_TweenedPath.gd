extends Node

# Called when the node enters the scene tree for the first time.
func _ready():
	$Menu.connect("changeTransitionType", self, "handle_change_transitionType")
	$Menu.connect("changeEasingType", self, "handle_change_easingType")


func handle_change_transitionType(transitionType):
	$TweenPath.set_transition_type(transitionType)
	
func handle_change_easingType(easingType):
	$TweenPath.set_easing_type(easingType)
